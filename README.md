# Proyectos de ley que son relevantes para la privacidad, libertad de expresión, acceso y cuestiones de género en el medio digital

Código fuente del sitio web (en Portugues) <https://radarlegislativo.org/>.

Mantenido por [Coding Rights](https://codingrights.org), una organización liderada por mujeres dedicada a promover la comprensión sobre el funcionamiento de las tecnologías digitales y exponer las asimetrías de poder que pueden ser ampliadas por su uso.

Nuestro trabajo implica el monitoreo y análisis de códigos legales, culturales y de programación para influenciar políticas públicas y fomentar buenas prácticas. Somos parte de una red global de activistas que crean y comparten herramientas y estrategias para el uso más autónomo y consciente de las tecnologías y para la inclusión de la perspectiva de los derechos humanos cuando se piensa en los medios digitales.

Si usted quiere entender el código fuente del sitio, vea (en portuges) [CONTRIBUTING.md][contributing].

[readme-en]: https://gitlab.com/codingrights/pls/blob/master/README.en.md
[contributing]: https://gitlab.com/codingrights/pls/blob/master/CONTRIBUTING.md
