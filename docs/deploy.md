Deploy do site para um ambiente de produção
===========================================

Pré-requisitos
--------------

O processo de deploy usa containers docker para criar um ambiente
facilmente reprodutível e estável. Por isso, é necessário [instalar o
docker][instalacao_docker] no seu servidor de produção. Todas as
outras dependências serão instaladas automaticamente em cada container.


Para facilitar o processo de deploy, você pode usar o [`Makefile` incluído][makefile]. Nesse caso, é necessário instalar o `make`. Em sistemas Debian GNU/Linux, basta executar


```
apt-get install make
```


Configurações
-------------

O ambiente de produção precisa de algumas configurações que não são
exatamente necessárias no ambiente de desenvolvimento. Elas devem ser
definidas em um arquivo `.env` na raiz do projeto (o mesmo diretório
que contém o `Makefile`).


As variáveis que precisam ser definidas são:

* `DEBUG` - em um ambiente de produção, ela deve ser definida como `False`

* `SECRET_KEY` - deve ser um valor único para cada instalação e deve ser mantido em segredo por [questões de segurança][django_secret_key].

* `TRAMITABOT_API_TOKEN` - o token de API fornecido pelo [@BotFather](https://telegram.me/BotFather) no [processo de criação do seu bot][telegram_bot].

* `POSTGRES_DB`, `POSTGRES_USER`, `POSTGRES_PASSWORD` - respectivamente, o nome, usuário e senha do banco de dados a ser usado.

* `HOST_DATABASE_FILES` - o caminho completo do diretório em que os arquivos do banco de dados estarão no host.

* `HOST_ELASTICSEARCH_FILES` - o caminho completo do diretório em que os arquivos do índice de texto completo estarão no host.


Deploy
------

Para fazer o deploy, basta estar no diretório raiz do projeto (o que
contém o `Makefile`) e executar

```
make deploy
```

Isso vai baixar todas as imagens do docker necessárias, instalar as
dependências e executar os serviços.


Rotinas de atualização
----------------------

Para manter as informações do projeto, é necessário rodar regularmente
o seguinte comando:

```
make update
```

Da mesma forma, para que o Tramitabot envie atualizações, é necessário
executar regularmente o seguinte comando:

```
make tramitabot_send_updates
```

Uma boa forma de agendar essas atualizações regulares é com o cron
(possívelmente usando usuários menos privilegiados, como descrito
abaixo).


Usando o cron com usuários menos privilegiados
----------------------------------------------

_Este passo é opcional, e só uma forma de configurar atualizações
regulares. Você pode pular essa etapa e fazer isso da forma que achar
melhor._

Para executar os comandos dentro dos containers docker, é necessário
que o usuário tenha permissão para executar os comandos do docker como
superusuário. Para executar esses comandos no cron, é necessário que
esse privilégio venha sem que uma senha tenha que ser fornecida.


Uma forma de reduzir o escopo de atuação possível desse usuário é
permitir que ele execute apenas essas ações como super usuário. Para
isso, crie os seguintes scripts:


`/usr/local/bin/update_projetos.sh`:
```
#!/bin/bash
cd <caminho_para_o_repositório>
/usr/local/bin/docker-compose run --rm web python manage.py update_all_projetos
```

`/usr/local/bin/send_tramitabot_updates.sh`:
```
#!/bin/bash
cd <caminho_para_o_repositório>
/usr/local/bin/docker-compose run --rm tramitabot python manage.py tramitabot_send_updates
```

Agora, basta adicionar o seguinte no seu arquivo `sudoers`:

```
# User alias specification
User_Alias RADAR_USERS = radarlegislativo

# Cmnd alias specification
Cmnd_Alias UPDATE_PROJETOS = /usr/local/bin/update_projetos.sh, /usr/loca/bin/send_tramitabot_updates.sh

# User privilege specification
RADAR_USERS ALL=(root) NOPASSWD: UPDATE_PROJETOS
```

E agendar as atualizações no cron:

```
00 05 * * * sudo /usr/local/bin/update_projetos.sh
00 10 * * 1-5 sudo /usr/local/bin/send_tramitabot_updates.sh
```



[instalacao_docker]: https://docs.docker.com/engine/installation/linux/docker-ce/debian/
[makefile]: https://gitlab.com/codingrights/radarlegislativo/blob/master/Makefile
[django_secret_key]: https://docs.djangoproject.com/en/1.11/ref/settings/#std:setting-SECRET_KEY
[telegram_bot]: https://core.telegram.org/bots#6-botfather
