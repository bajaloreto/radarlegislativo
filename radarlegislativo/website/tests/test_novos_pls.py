# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import datetime

from django.test import TestCase, mock
from django.urls import reverse
from freezegun import freeze_time
from model_mommy import mommy

from main.models import Projeto


__all__ = ["TestPaginaNovosPLs"]


class TestPaginaNovosPLs(TestCase):
    fixtures = ["projetos", "tramitacoes", "tags"]

    def setUp(self):
        self.projeto = Projeto.publicados.first()

    def test_pagina_de_novos_PLs_retorna_200(self):
        response = self.client.get(reverse("website:novos_pls"))
        self.assertEqual(response.status_code, 200)

    def test_pagina_de_novos_pls_usa_o_template_certo(self):
        response = self.client.get(reverse("website:novos_pls"))
        self.assertTemplateUsed(response, "website/novos_pls.html")

    @mock.patch('website.views.Projeto')
    def test_inclui_projetos_da_semana_selecionada(self, mocked_model):
        self.client.get(reverse("website:novos_pls"), {"data": "13/09/2017"})

        mocked_model.publicados.na_mesma_semana_que_o_dia\
            .assert_called_once_with(datetime.date(2017, 9, 13))
        mocked_model.publicados.all.assert_not_called()

    @mock.patch('website.views.Projeto')
    def test_usa_ultima_semana_com_novos_pls_se_data_nao_foi_definida(self,
                                                            mocked_model):
        with freeze_time('2018-05-10'):
            projeto_mais_novo = mommy.make(Projeto,
                                           cadastro=datetime.date(2018, 5, 10))

        mocked_model.publicados.mais_novo.return_value = projeto_mais_novo

        self.client.get(reverse("website:novos_pls"))
        mocked_model.publicados.na_mesma_semana_que_o_dia\
            .assert_called_once_with(projeto_mais_novo.cadastro)
        mocked_model.publicados.all.assert_not_called()

    def test_filtra_por_origem_se_o_parametro_de_GET_for_fornecido(self):
        response = self.client.get(
            reverse("website:novos_pls"),
            {"data": "13/09/2017", "origem": "SE"})
        eventos_esperados = Projeto.publicados.\
            na_mesma_semana_que_o_dia(datetime.date(2017, 9, 13))\
            .filter(origem="SE").order_by("cadastro")
        self.assertEqual(list(eventos_esperados),
                         list(response.context["novos_pls"]))

    def test_projeto_nao_publicado_nao_e_listado(self):
        response = self.client.get(
            reverse("website:novos_pls"),
            {"data": "17/11/2015"}
        )
        self.assertContains(
            response,
            u"Não existem projetos novos na semana selecionada."
        )
