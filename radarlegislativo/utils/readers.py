import codecs
import yaml


class YamlReader:

    @classmethod
    def read(cls, filename):
        with codecs.open(filename, encoding='utf-8') as yaml_file:
            return yaml.load(yaml_file)
