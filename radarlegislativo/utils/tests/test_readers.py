from mock import Mock, patch
from unittest import TestCase

from utils.readers import YamlReader


class YamlReaderTestCase(TestCase):

    def setUp(self):
        self.reader = YamlReader
        self.m_codec, self.m_yaml = [
            patch('utils.readers.codecs', spec=True, create=True).start(),
            patch('utils.readers.yaml', spec=True).start(),
        ]

    def test_read_calls_libs_correctly(self):
        m_file = Mock()
        m_file.__enter__ = Mock(return_value='Hop')
        m_file.__exit__ = Mock(return_value=False)
        self.m_codec.open.return_value = m_file
        self.m_yaml.load.return_value = 'Pas'

        data = self.reader.read('filename')

        self.m_codec.open.assert_called_once_with('filename', encoding='utf-8')
        self.m_yaml.load.assert_called_once_with('Hop')
        assert data == 'Pas'
