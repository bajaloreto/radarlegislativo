import chardet
import codecs


def decode(text):
    try:
        encoding = chardet.detect(text)['encoding']
    except TypeError:
        return text
    return codecs.decode(text, encoding)
