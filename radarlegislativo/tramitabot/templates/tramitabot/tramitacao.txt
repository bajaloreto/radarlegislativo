
*{{ tramitacao.data|date:"d/m/Y" }}*
[{% if tramitacao.projeto.apelido %}{{ tramitacao.projeto.apelido }}{% else %}{{ tramitacao.projeto }}{% endif %}](https://www.radarlegislativo.org{{ tramitacao.get_absolute_url }})

_Ementa_: {{ tramitacao.projeto.ementa|safe }}

_Tramitação_: {{ tramitacao.descricao_limpa|safe }}
