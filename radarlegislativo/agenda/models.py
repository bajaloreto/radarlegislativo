# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible

from markdownx.models import MarkdownxField

from main.models import Projeto
from parlamento.models import Comissao


class EventoManager(models.Manager):
    def na_mesma_semana_que_o_dia(self, dia):
        semana = dia.isocalendar()[1]
        return self.filter(data__week=semana, data__year=dia.year)


@python_2_unicode_compatible
class Evento(models.Model):

    objects = EventoManager()

    data = models.DateField()
    link = models.URLField(blank=True)
    descricao = MarkdownxField()
    comissao = models.ForeignKey(Comissao)

    class Meta:
        ordering = ['data']

    def __str__(self):
        return u"{} - {}".format(self.data, self.comissao)
