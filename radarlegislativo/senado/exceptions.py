class ClientConnectionError(Exception):
    '''
        Exception to be raised when senado API response contains an error.
    '''
