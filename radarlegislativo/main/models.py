#-*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import unicode_literals
import datetime
import os

from django.core.exceptions import ValidationError
from django.db import models
from django.dispatch import receiver
from django.urls import reverse
from django.utils.encoding import python_2_unicode_compatible
from django.utils.text import slugify

from markdownx.models import MarkdownxField

URL_API_SENADO_MATERIA = u'http://legis.senado.leg.br/dadosabertos/materia/{}'
URL_API_SENADO_MOVIMENTACOES = u'http://legis.senado.leg.br/dadosabertos/materia/movimentacoes/{}'
URL_WEB_SENADO = u'http://www25.senado.leg.br/web/atividade/materias/-/materia/{}'

URL_API_CAMARA = u'https://www.camara.leg.br/SitCamaraWS/Proposicoes.asmx?WSDL'
URL_API_CAMARA_LOCATION = u'https://www.camara.leg.br/SitCamaraWS/Proposicoes.asmx'
URL_WEB_CAMARA = u'https://www.camara.leg.br/proposicoesWeb/fichadetramitacao?idProposicao={}'


@python_2_unicode_compatible
class Tag(models.Model):
    """Tags são categorias mais amplas do que palavras-chave, utilizadas para
    organizar e filtrar os projetos de lei na interface"""
    nome = models.CharField('tag', max_length=256)
    slug = models.SlugField(max_length=20, unique=True)
    icon = models.FileField(upload_to='tags-icons', null=True, blank=True)

    def __str__(self):
        return self.nome


@python_2_unicode_compatible
class PalavraChave(models.Model):
    """Palavras-chave são mais granulares do que as tags, normalmente vem como
    parâmetro quando adiciona-se um projeto de lei automaticamente (por
    exemplo, com o Raspador Legislativo). Armazena-se as palavras-chave para
    fins de busca, de mapear o porquê de um projeto ter sido adicionado e para
    podermos relacionar documentos a diversos projetos de lei de forma mais
    granular do que via tags."""
    nome = models.CharField('palavra-chave', max_length=256, unique=True)

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = 'palavra-chave'
        verbose_name_plural = 'palavras-chave'
        ordering = ('nome',)


class ProjetoManager(models.Manager):

    def na_mesma_semana_que_o_dia(self, dia):
        semana = dia.isocalendar()[1]
        return self.filter(cadastro__week=semana)

    def mais_novo(self):
        return self.order_by('-cadastro').first()

    def novos(self):
        return self.na_mesma_semana_que_o_dia(self.mais_novo().cadastro)


class ProjetosPublicadosManager(ProjetoManager):

    def get_queryset(self):
        qs = super(ProjetosPublicadosManager, self).get_queryset()
        return qs.filter(publicado=True)


class ProjetosNaoPublicadosManager(ProjetoManager):

    def get_queryset(self):
        qs = super(ProjetosNaoPublicadosManager, self).get_queryset()
        return qs.filter(publicado=False)


class ProjetosPublicadosManager(ProjetoManager):

    def get_queryset(self):
        qs = super(ProjetosPublicadosManager, self).get_queryset()
        return qs.filter(publicado=True)


class ProjetosNaoPublicadosManager(ProjetoManager):

    def get_queryset(self):
        qs = super(ProjetosNaoPublicadosManager, self).get_queryset()
        return qs.filter(publicado=False)


@python_2_unicode_compatible
class Projeto(models.Model):

    objects = ProjetoManager()
    publicados = ProjetosPublicadosManager()
    nao_publicados = ProjetosNaoPublicadosManager()

    CAMARA = "CA"
    SENADO = "SE"

    ORIGEM_CHOICES = (
        (CAMARA, "Câmara"),
        (SENADO, "Senado"),
    )

    VERDE = 1
    AMARELO = 2
    VERMELHO = 3

    IMPACTO_CHOICES = (
        (VERDE, '🙂️'),
        (AMARELO, "😐"),
        (VERMELHO, "🙁")
    )

    origem = models.CharField(max_length=2, choices=ORIGEM_CHOICES)
    id_site = models.IntegerField('id no site de origem')
    nome = models.CharField(max_length=255)
    tags = models.ManyToManyField(Tag)
    palavras_chave = models.ManyToManyField(PalavraChave, blank=True)

    apresentacao = models.DateField()
    ementa = models.TextField()
    autoria = models.CharField(max_length=255)
    apensadas = models.TextField(blank=True)
    local = models.TextField(blank=True)

    html_original = models.TextField(blank=True)

    apelido = models.CharField(max_length=255, blank=True)
    importante = models.BooleanField(default=False)
    urgente = models.BooleanField(default=False)
    arquivado = models.BooleanField(default=False)
    promulgado = models.BooleanField(default=False)
    impacto = models.IntegerField(choices=IMPACTO_CHOICES, default=AMARELO)
    informacoes_adicionais = MarkdownxField(blank=True)
    clippings = MarkdownxField(blank=True)
    pontos_principais = MarkdownxField(blank=True)
    texto_acao = models.TextField(blank=True)
    publicado = models.BooleanField(db_index=True, default=True)

    nome_da_lei_promulgada = models.CharField(max_length=255, blank=True,
            help_text="Nome da lei caso o projeto tenha sido promulgado")
    link_da_lei_promulgada = models.TextField(blank=True,
            help_text="Link para o texto da lei caso o projeto tenha "
            "sido promulgado")

    publicado = models.BooleanField(db_index=True, default=True)

    cadastro = models.DateField(auto_now_add=True)

    ultima_atualizacao = models.DateField(default=datetime.date.today)

    texto_acao.verbose_name = "texto de ação"

    class Meta:
        unique_together = (("origem", "id_site"),)
        ordering = ['-ultima_atualizacao']

    def tag_list(self):
        """
        Shows a list of tags for this project.

        We use this for the list_display in ProjetoAdmin, but it adds another
        (potentially big) query to that page. If it grows too slow, we can
        remove this (and not have a list of tags in the admin list_display).
        """
        return ', '.join(self.tags.values_list('nome',
            flat=True).order_by('nome'))

    @property
    def ultima_tramitacao(self):
        return self.tramitacao_set.latest()

    @property
    def ultimas_tramitacoes(self):
        return self.tramitacao_set.order_by('-data')

    @property
    def slug(self):
        return slugify(self.nome)

    @property
    def situacao(self):
        return "{} - {}".format(self.ultima_tramitacao.local, self.ultima_tramitacao.descricao)

    @property
    def link(self):
        if self.origem == self.SENADO:
            return URL_WEB_SENADO.format(self.id_site)
        elif self.origem == self.CAMARA:
            return URL_WEB_CAMARA.format(self.id_site)

    @property
    def documentos(self):
        if hasattr(self, '_documentos'):
            return self._documentos

        self._documentos = Documento.objects.filter(
            models.Q(tags__in=self.tags.all()) |
            models.Q(palavras_chave__in=self.palavras_chave.all()) |
            models.Q(projetos=self)
        )
        return self._documentos

    def link_para_edicao(self):
        url_name =  "admin:{}_{}_change".format(self._meta.app_label, self._meta.model_name)
        return reverse(url_name, args=(self.id,))

    def get_absolute_url(self):
        return reverse("website:projeto", args=[self.pk])

    def __str__(self):
        return '{} - {}'.format(self.nome, self.get_origem_display())


@python_2_unicode_compatible
class Tramitacao(models.Model):
    id_site = models.CharField(max_length=255)
    data = models.DateField()
    projeto = models.ForeignKey(Projeto)
    local = models.CharField(max_length=255)
    descricao = models.TextField()

    class Meta:
        ordering = ["-data"]
        get_latest_by = "data"
        verbose_name_plural = "tramitações"

    def save(self, *args, **kwargs):
        result = super(Tramitacao, self).save(*args, **kwargs)
        self.projeto.ultima_atualizacao = self.data
        self.projeto.save()
        return result

    def get_absolute_url(self):
        return self.projeto.get_absolute_url()

    @property
    def descricao_limpa(self):
        string_to_remove = 'Inteiro teor'
        if self.descricao.endswith(string_to_remove):
            return self.descricao[:-len(string_to_remove)]
        return self.descricao

    def __str__(self):
        return u'{} - {} - {}'.format(self.projeto, self.data, self.local)


@python_2_unicode_compatible
class Documento(models.Model):
    titulo = models.CharField('Título', max_length=255)
    projetos = models.ManyToManyField(Projeto, blank=True)
    tags = models.ManyToManyField(Tag, blank=True)
    palavras_chave = models.ManyToManyField(PalavraChave, blank=True)
    criado_em = models.DateTimeField(auto_now_add=True, db_index=True)
    arquivo = models.FileField(upload_to='documentos', blank=True, null=True)
    url = models.URLField('URL', blank=True, null=True)

    # Forca um Documento a ter arquivo ou URL
    def clean(self):
        if not self.arquivo and not self.url:
            raise ValidationError(
            'Um documento deve ter Arquivo ou URL. Preencha um dos dois campos.'
            )
        if self.arquivo and self.url:
            raise ValidationError(
            'Um documento deve ter Arquivo ou URL. Apague um dos campos.'
            )

    def get_url(self):
        return self.url if self.url else self.arquivo.url

    def __str__(self):
        return u'{} - {}'.format(self.titulo, self.arquivo)

    class Meta:
        ordering = ['-criado_em']

@receiver(models.signals.post_delete, sender=Documento)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    if instance.arquivo:
        if os.path.isfile(instance.arquivo.path):
            os.remove(instance.arquivo.path)
