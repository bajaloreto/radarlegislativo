from datetime import datetime
from pytz import timezone

from utils.generics import decode


class CamaraProjectMapper:

    @classmethod
    def map(cls, data):
        data = data['proposicao']
        kwargs = {
            'nome': decode(data.get('nomeProposicao', '')),
            'autoria': decode(data.get('Autor', ''))[:250],
            'ementa': decode(data.get('Ementa', '')),
        }

        apr = decode(data.get('DataApresentacao', ''))
        kwargs['apresentacao'] = datetime.strptime(
            apr, '%d/%m/%Y'
        ).replace(tzinfo=timezone('America/Sao_Paulo'))

        kwargs['apensadas'] = ApensadaMapper.map(data)

        return kwargs


class ApensadaMapper:

    @classmethod
    def map(cls, data):
        mapped = []
        apensadas = data.get('apensadas', {})

        for item in ProposicaoMapper.map(apensadas):
            mapped.append(item)
        return ', '.join(mapped)


class ProposicaoMapper:

    @classmethod
    def map(cls, data):
        if 'nomeProposicao' in data:
            yield decode(data['nomeProposicao'])
        elif 'proposicao' in data:
            for mapped in cls.map(data['proposicao']):
                yield mapped
        else:
            for item in data:
                for mapped in cls.map({item[0]: item[1]}):
                    yield mapped
